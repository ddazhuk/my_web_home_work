package lesson2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class First_Test {
    String login="ddazhuk";
    String password="SSSSS";
    String project="MAS_1003A1";//BNM_12_SRR or MAS_1003A1
    WebDriver driver=new ChromeDriver();
    @Test
    public void testlogin(){
        try {
            driver.get("https://hrk-cv-dev6f.axiomsl.us:8081/cv/ui/global/index");
            WebElement element = driver.findElement(By.name("j_username"));
            element.sendKeys(login);
            element = driver.findElement(By.name("j_password"));
            element.sendKeys(password);
            element = driver.findElement(By.className("login-submit"));
            element.click();
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.findElement(By.id("gwt-uid-8")).click();
            driver.findElement(By.id("ic-logout-last_Log_out")).click();
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
            Assert.assertTrue(driver.findElement(By.id("login-container")).isDisplayed());
        }finally {
            driver.quit();
        }

    }
    @Test
    public void testDBsource(){
        try {
            driver.get("https://hrk-cv-dev6f.axiomsl.us:8081/cv/ui/global/index");
            driver.findElement(By.name("j_username")).sendKeys(login);
            driver.findElement(By.name("j_password")).sendKeys(password);
            driver.findElement(By.className("login-submit")).click();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.findElement(By.xpath("//div[@id='ic-hamburger-menu']/span[@class='v-button-wrap']")).click();
            //driver.findElement(By.xpath("//div[@id=\"ic-hamburger-menu\"]//*[name()=\"svg\"]")).click();
            driver.findElement(By.id("ic-global-setting_System_Settings")).click();
            driver.findElement(By.id("DB_Source")).click();
            Assert.assertTrue(driver.findElement(By.xpath("//body[@class='v-generated-body v-sa v-ch v-webkit v-win']")).isDisplayed());
        }finally {
            driver.quit();
        }
    }
    @Test
    public void testProject(){
        try {
            driver.get("https://hrk-cv-dev6f.axiomsl.us:8081/cv/ui/global/index");
            driver.findElement(By.name("j_username")).sendKeys(login);
            driver.findElement(By.name("j_password")).sendKeys(password);
            driver.findElement(By.className("login-submit")).click();
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
            driver.findElement(By.id("ALL_BRANCHES")).click();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            driver.findElement(By.xpath("//*[contains(@class,'split-pane-component-1')]//*[@id=\"ic-lookup-big\"]")).click();
            driver.findElement(By.xpath("//input[@type='text']")).sendKeys(project);
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            //Assert.assertTrue(driver.findElement(By.xpath("//span[ .='"+project+"']")).isDisplayed());
            Assert.assertTrue(driver.findElement(By.xpath("//div[@class='cv-gbft-tree-node-object']//span[.='"+project+"']")).isDisplayed());
        }finally {
            driver.quit();
        }

    }
}
/*WebDriver driver= new ChromeDriver();
        driver.get("https://www.ukr.net/");*/
        /*driver.switchTo().frame("mail widget");
        WebElement ele=driver.findElement(By.id("id-input-login"));
        ele.sendKeys("777777");
        WebElement element = driver.findElement(By.id("id-input-password"));
        element.sendKeys("test");*/
/////////////////////////////////////////
        /*List<WebElement> one= driver.findElements(By.className("feed__item"));
        System.out.println(one.size());
        driver.quit();
        System.out.println("end_FF");*/
/////////////////////////////////////////////
        /*List<WebElement> one= driver.findElements(By.tagName("img"));
        System.out.println(one.size());
        driver.quit();
        System.out.println("end_FF");*/
       /* driver.switchTo().frame("mail widget");
        WebElement welem=driver.findElement(By.tagName("input"));
        welem.sendKeys("888");
        WebElement welem1=driver.findElement(By.tagName("input"));
        welem.sendKeys("999");*/