package lesson3;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Testclass {



    @DataProvider
    public Object[][]getData(){
        return  new Object[][]{
                {"user1","pasw"},
                {"user2","pasw2"},
                {"user3","pas3"},

        };
    }
    @Test(dataProvider="getData")
    public void lessonTest(String username,String password){
        System.out.println(username + password);

    }
    @BeforeMethod
    public void before(){
        System.out.println("Before");
    };
    @Test
    public void One(){
        System.out.println("One");
    }
    @Test
    public void Two(){
        System.out.println("Two");
    }
}
