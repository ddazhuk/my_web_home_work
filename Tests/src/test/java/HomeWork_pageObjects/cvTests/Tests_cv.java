package HomeWork_pageObjects.cvTests;
import HomeWork_pageObjects.pageObjects.BasePage;
import HomeWork_pageObjects.pageObjects.ListofProjects;
import HomeWork_pageObjects.pageObjects.LoginPage;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Tests_cv extends BasePage {
    @Test
    public void logIn(){
        LoginPage one=new LoginPage();
        driver=one.login("ddazhuk","sssssss").logout();
        Assert.assertTrue(driver.findElement(By.id("login-container")).isDisplayed());
    }
    @Test
    public void dbSource(){
        LoginPage two=new LoginPage();
        driver=two.login("ddazhuk","sssssss").wayToDBsource();
        Assert.assertTrue(driver.findElement(By.xpath("//body[@class='v-generated-body v-sa v-ch v-webkit v-win']")).isDisplayed());
    }
    @Test
    public void lookForPrj(){
        String name_project="MAS_1003A1";
        ListofProjects three = new ListofProjects();
        three.login("ddazhuk","sssssss");
        driver=three.lookForProject(name_project);
        Assert.assertTrue(driver.findElement(By.xpath("//div[@class='cv-gbft-tree-node-object']//span[.='"+name_project+"']")).isDisplayed());
    }
}
