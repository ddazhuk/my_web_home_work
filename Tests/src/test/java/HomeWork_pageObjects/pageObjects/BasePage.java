package HomeWork_pageObjects.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

public class BasePage {
    public WebDriver driver;
    public WebDriver getDriver(){
        driver=new ChromeDriver();
        return driver;
    }
    public BasePage openHD(String url){
        getDriver().get(url);
        return this;
    }
    @AfterMethod
    public void closeHD(){
        driver.quit();
    }
}
