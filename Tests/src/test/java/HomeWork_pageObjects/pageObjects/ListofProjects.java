package HomeWork_pageObjects.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class ListofProjects extends LoginPage {
    By allbranch=By.id("ALL_BRANCHES");
    By search=By.xpath("//*[contains(@class,'split-pane-component-1')]//*[@id=\"ic-lookup-big\"]");
    By imputrow=By.xpath("//input[@type='text']");

    public WebDriver lookForProject(String project){
        driver.findElement(allbranch).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(search).click();
        driver.findElement(imputrow).sendKeys(project);
        return driver;
    }
}
