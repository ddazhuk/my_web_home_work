package HomeWork_pageObjects.pageObjects;

import org.openqa.selenium.By;

public class LoginPage extends Footer {
    String url="https://hrk-cv-dev6f.axiomsl.us:8081/cv/ui/global/index";
    By usernam=By.name("j_username");
    By paswor=By.name("j_password");
    By subButtton=By.className("login-submit");
    public By element_For_assert=By.id("login-container");

    public LoginPage login(String usnm,String psw){
        openHD(url);
        driver.findElement(usernam).sendKeys(usnm);
        driver.findElement(paswor).sendKeys(psw);
        driver.findElement(subButtton).click();
        driver.manage().window().maximize();
        return this;
    }
}
