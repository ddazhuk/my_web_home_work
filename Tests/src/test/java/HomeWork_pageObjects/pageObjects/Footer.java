package HomeWork_pageObjects.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class Footer extends BasePage {
    public By preLogButt=By.id("gwt-uid-8");
    public By logoutButt=By.id("ic-logout-last_Log_out");
    public By burgerMeny=By.xpath("//div[@id='ic-hamburger-menu']/span[@class='v-button-wrap']");
    public By globSet=By.id("ic-global-setting_System_Settings");
    public By dbSourcButton=By.id("DB_Source");
    public WebDriver logout(){
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(preLogButt).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(logoutButt).click();
        return driver;
    }
    public WebDriver wayToDBsource(){
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(burgerMeny).click();
        driver.findElement(globSet).click();
        driver.findElement(dbSourcButton).click();
        return driver;
    }
}
